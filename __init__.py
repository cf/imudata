from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m import logging
from ctx import Context
import sys
import os
import st3m.run


log = logging.Log(__name__, level=logging.INFO)
log.info("import")

UPDATE_TIMEOUT = 500

class imuData(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.accx = 0
        self.accy = 0
        self.accz = 0
        self.gyrox = 0
        self.gyroy = 0
        self.gyroz = 0
        self.temp = 0
        self.press = 0
        self.accxmin = 0
        self.accymin = 0
        self.acczmin = 0
        self.gyroxmin = 0
        self.gyroymin = 0
        self.gyrozmin = 0
        self.tempmin = 0
        self.pressmin = 0
        self.accxmax = 0
        self.accymax = 0
        self.acczmax = 0
        self.gyroxmax = 0
        self.gyroymax = 0
        self.gyrozmax = 0
        self.tempmax = 0
        self.pressmax = 0
        self.curpage = 0
        self.ptitle = ['live ->', '<- min ->', '<- max']
        self.last_pressed = 0
        self.timeractive = 0

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.font_size = 20
        ctx.text_align = ctx.MIDDLE
        ctx.move_to(0 , -90).rgb(0,255,255).text(str(self.ptitle[self.curpage]))
        ctx.text_align = ctx.LEFT
        ctx.move_to(-80, -70).rgb(0, 255, 255).text('AccX')
        ctx.move_to(-80, -50).rgb(0, 255, 255).text('AccY')
        ctx.move_to(-80, -30).rgb(0, 255, 255).text('AccZ')
        ctx.move_to(-80, 0).rgb(0, 255, 255).text('GyroX')
        ctx.move_to(-80, 20).rgb(0, 255, 255).text('GyroY')
        ctx.move_to(-80, 40).rgb(0, 255, 255).text('GyroZ')
        #ctx.move_to(-80, 70).rgb(0, 255, 255).text('Temp')
        ctx.move_to(-80, 90).rgb(0, 255, 255).text('Press.')

        ctx.move_to(-20, -70).rgb(255, 255, 255).text(str(self.accx))
        ctx.move_to(-20, -50).rgb(255, 255, 255).text(str(self.accy))
        ctx.move_to(-20, -30).rgb(255, 255, 255).text(str(self.accz))
        ctx.move_to(-20, 0).rgb(255, 255, 255).text(str(self.gyrox))
        ctx.move_to(-20, 20).rgb(255, 255, 255).text(str(self.gyroy))
        ctx.move_to(-20, 40).rgb(255, 255, 255).text(str(self.gyroz))
        #ctx.move_to(-20, 70).rgb(255, 255, 255).text(str(self.temp))
        ctx.move_to(-20, 90).rgb(255, 255, 255).text(str(self.press))
 
    def update_input(self, ins: InputState):
        if self.accx > self.accxmax or self.accxmax == 0:
            self.accxmax = self.accx
        if self.accx < self.accxmin or self.accxmin == 0:
            self.accxmin = self.accx
        if self.accy > self.accymax or self.accymax == 0:
            self.accymax = self.accy
        if self.accy < self.accymin or self.accymin == 0:
            self.accymin = self.accy
        if self.accz > self.acczmax or self.acczmax == 0:
            self.acczmax = self.accz
        if self.accz < self.acczmin or self.acczmin == 0:
            self.acczmin = self.accz        

        if self.gyrox > self.gyroxmax or self.gyroxmax == 0:
            self.gyroxmax = self.gyrox
        if self.gyrox < self.gyroxmin or self.gyroxmin == 0:
            self.gyroxmin = self.gyrox
        if self.gyroy > self.gyroymax or self.gyroymax == 0:
            self.gyroymax = self.gyroy
        if self.gyroy < self.gyroymin or self.gyroymin == 0:
            self.gyroymin = self.gyroy
        if self.gyroz > self.gyrozmax or self.gyrozmax == 0:
            self.gyrozmax = self.gyroz
        if self.gyroz < self.gyrozmin or self.gyrozmin == 0:
            self.gyrozmin = self.gyroz

        if self.press > self.pressmax or self.pressmax == 0:
            self.pressmax = self.press    
        if self.press < self.pressmin or self.pressmin == 0:
            self.pressmin = self.press    

        if self.curpage == 0:
            self.accx = ins.imu.acc[0]
            self.accy = ins.imu.acc[1]
            self.accz = ins.imu.acc[2]
            self.gyrox = ins.imu.gyro[0]
            self.gyroy = ins.imu.gyro[1]
            self.gyroz = ins.imu.gyro[2]
            self.press = ins.imu.pressure
            #self.temp = ins.imu.temperature
            #log.info(ins.imu.__dict__)

        if self.curpage == 1:
            self.accx = self.accxmin
            self.accy = self.accymin
            self.accz = self.acczmin
            self.gyrox = self.gyroxmin
            self.gyroy = self.gyroymin
            self.gyroz = self.gyrozmin
            self.press = self.pressmin
            #self.temp = ins.imu.temperature
            #log.info(ins.imu.__dict__)

        if self.curpage == 2:
            self.accx = self.accxmax
            self.accy = self.accymax
            self.accz = self.acczmax
            self.gyrox = self.gyroxmax
            self.gyroy = self.gyroymax
            self.gyroz = self.gyrozmax
            self.press = self.pressmax
            #self.temp = ins.imu.temperature
            #log.info(ins.imu.__dict__)

        
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        
        self.update_input(ins)
        direction = ins.buttons.app # -1 (left), 1 (right), or 2 (pressed)

        if self.last_pressed == 0:
            if direction == ins.buttons.PRESSED_LEFT and self.curpage > 0:
                self.curpage -= 1
                self.timeractive = 1
            
            elif direction == ins.buttons.PRESSED_RIGHT and self.curpage < 2:
                self.curpage += 1
                self.timeractive = 1
            
            elif direction == ins.buttons.PRESSED_DOWN:
                if self.curpage == 1:
                    self.accxmin = 0
                    self.accymin = 0
                    self.acczmin = 0
                    self.gyroxmin = 0
                    self.gyroymin = 0
                    self.gyrozmin = 0
                    self.tempmin = 0
                    self.pressmin = 0
                    self.accx = 0
                    self.accy = 0
                    self.accz = 0
                    self.gyrox = 0
                    self.gyroy = 0
                    self.gyroz = 0
                    self.temp = 0
                    self.press = 0
                if self.curpage == 2:
                    self.accxmax = 0
                    self.accymax = 0
                    self.acczmax = 0
                    self.gyroxmax = 0
                    self.gyroymax = 0
                    self.gyrozmax = 0
                    self.tempmax = 0
                    self.pressmax = 0
                    self.accx = 0
                    self.accy = 0
                    self.accz = 0
                    self.gyrox = 0
                    self.gyroy = 0
                    self.gyroz = 0
                    self.temp = 0
                    self.press = 0

        if self.timeractive:
            self.last_pressed += delta_ms
        
            if self.last_pressed > UPDATE_TIMEOUT:
                self.last_pressed = 0
                self.timeractive = 0

if __name__ == '__main__':
    st3m.run.run_view(imuData(ApplicationContext()))

